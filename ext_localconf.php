<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RZ.' . $extKey,
            'Carousel',
            array(
                'Carousel' => 'list',

            ),
            // non-cacheable actions
            array(
                'Carousel' => '',

            )
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RZ.' . $extKey,
            'Content',
            array(
                'Content' => 'list',

            ),
            // non-cacheable actions
            array(
                'Content' => '',

            )
        );

        if (TYPO3_MODE === 'BE') {
            $icons = [
                'ext-rzslick-wizard-icon' => 'ce_wiz.svg',
            ];
            $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
            foreach ($icons as $identifier => $path) {
                $iconRegistry->registerIcon(
                    $identifier,
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:rzslick/Resources/Public/Icons/' . $path]
                );
            }
        }

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    rzslick_carousel {
                        iconIdentifier = ext-rzslick-wizard-icon
                        title = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_carousel_pluginWizardTitle
                        description = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_carousel_pluginWizardDescription
                        tt_content_defValues {
                            CType = list
                            list_type = rzslick_carousel
                        }
                    }

                    rzslick_content {
                        iconIdentifier = ext-rzslick-wizard-icon
                        title = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_content_pluginWizardTitle
                        description = LLL:EXT:rzslick/Resources/Private/Language/locallang_be.xlf:rzslick_content_pluginWizardDescription
                        tt_content_defValues {
                            CType = list
                            list_type = rzslick_content
                        }
                    }
                }
                show = *
            }
       }'
        );

    },
    $_EXTKEY
);
