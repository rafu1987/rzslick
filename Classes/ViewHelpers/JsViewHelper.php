<?php
namespace RZ\Rzslick\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\Asset;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Build JavaScript
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class JsViewHelper extends AbstractViewHelper
{

    /**
     * Add JS
     *
     * @param array $settings
     * @param string $prevNext
     * @param string $uid
     * @param string $selector
     * @return string
     */
    public function render($settings, $prevNext, $uid, $selector)
    {
        // Edit settings
        $settings = $this->editSettings($settings, $prevNext);

        // Output JS
        return $this->js($settings, $uid, $selector);
    }

    /**
     * Filter settings
     *
     * @param array $settings
     * @param string $prevNext
     * @return string
     */
    protected function editSettings($settings, $prevNext)
    {
        // Remove settings
        unset($settings['images']);
        unset($settings['addJquery']);
        unset($settings['addSlick']);
        unset($settings['slideItems']);
        unset($settings['templateLayout']);
        unset($settings['pages']);

        foreach ($prevNext as $key => $val) {
            $settings[$key] = "'" . $val . "'";
        }

        // Output settings
        return $settings;
    }

    /**
     * Build JS
     *
     * @param array $settings
     * @param string $uid
     * @param string $selector
     * @return string
     */
    protected function js($settings, $uid, $selector)
    {
        // Responsive
        if ($settings['responsive']) {
            $itemsArr = array(
                '1200' => $settings['slidesToShow'],
                '992' => $settings['items992'],
                '768' => $settings['items768'],
            );

            $items = '';
            foreach ($itemsArr as $itemKey => $itemVal) {
                $items .= '
										{
											breakpoint: ' . $itemKey . ',
											settings: {
												slidesToShow: ' . $itemVal . '
											}
										},
								';
            }

            $responsive = '
								,responsive: [ ' . $items . ' ]
						';
        }

        unset($settings['responsive']);
        unset($settings['items992']);
        unset($settings['items768']);

        if ($settings['dotsEach'] == 0) {
            unset($settings['dotsEach']);
        }

        $slickSettings = '';
        $keys = array(
            'autoplay',
            'pauseOnHover',
            'arrows',
            'dots',
            'infinite',
            'autoWidth',
            'draggable',
            'touchMove',
            'fade',
            'adaptiveHeight',
            'variableWidth',
        );
        foreach ($settings as $key => $val) {
            if (in_array($key, $keys)) {
                if ($val == 1) {
                    $val = 'true';
                } else {
                    $val = 'false';
                }

            }

            if ($key == 'initialSlide') {
                if ($val != 0) {
                    $val = $val - 1;
                }

            }

            $slickSettings .= $key . ':' . $val . ',';
        }

        // Remove last comma
        $slickSettings = substr($slickSettings, 0, -1);

        Asset::createFromSettings(array(
            'name' => 'rzslick' . $uid,
            'dependencies' => 'rzslick_slick',
            'path' => 'EXT:rzslick/Resources/Public/Js/rzslick.js',
            'fluid' => true,
            'variables' => array(
                'selector' => $selector,
                'slickSettings' => $slickSettings,
                'responsive' => $responsive,
            ),
        ));
    }

}
