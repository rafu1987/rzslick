<?php
namespace RZ\Rzslick\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\Asset;
use RZ\Rzslick\Utility\T3jquery;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Content controller
 *
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 */
class ContentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * itemsRepository
     *
     * @var \RZ\Rzslick\Domain\Repository\ItemsRepository
     * @inject
     */
    protected $itemsRepository = null;

    /**
     * List action
     *
     * @return void
     */
    public function listAction()
    {
        // t3jquery
        $t3jqueryCheck = T3jquery::check();

        // Add jQuery
        if ($t3jqueryCheck === false) {
            if ($this->settings['addJquery']) {
                Asset::createFromSettings(array(
                    'name' => 'rzslick_jquery',
                    'path' => 'EXT:rzslick/Resources/Public/Js/jquery-1.12.3.min.js',
                ));
            }
        }

        // Get uid
        $cObj = $this->configurationManager->getContentObject();
        $uid = $cObj->data['uid'];

        // Get offers from settings
        $slider = explode(',', $this->settings['slideItems']);

        if ($slider) {
            $items = array();
            foreach($slider as $slide) {
                $items[] = $this->itemsRepository->findByUid($slide);
            }

            $this->view->assign('items', $items);

            // Set CE uid
            $this->view->assign('uid', $uid);

            // Pages
            $pages = $this->settings['pages'];

            if ($pages != '') {
                // Check if there are any slides on the defined pages
                $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid', 'tx_rzslick_domain_model_items', 'hidden=0 AND deleted=0 AND pid=' . $pages, '', '', '');
                $num = $GLOBALS['TYPO3_DB']->sql_num_rows($res);

                if (!$num) {
                    $this->addFlashMessage($this->__('warningText'), $messageTitle = $this->__('warning'), $severity = AbstractMessage::WARNING, $storeInSession = true);
                }
            } else {
                $this->addFlashMessage($this->__('informationText'), $messageTitle = $this->__('information'), $severity = AbstractMessage::INFO, $storeInSession = true);
            }
        }
    }

    /**
     * Translate function
     *
     * @param string $key
     * @param array $vars
     * @return void
     */
    protected function __($key, $vars = array())
    {
        if (empty($vars)) {
            return LocalizationUtility::translate($key, $this->extensionName);
        } else {
            return vsprintf(LocalizationUtility::translate($key, $this->extensionName), (array) $vars);
        }
    }

}
